app.controller('LivrosController', function($scope, $state, $stateParams, LivrosService) {
    var _self = this;

    $scope.salvar = function(){
        if($scope.livro.id){
            _self.edit($scope.livro);
        } else {
            _self.add($scope.livro);
        }
    }

    $scope.excluir = function(livroId){
        _self.delete(livroId);
    };

    $scope.adicionar = function(){
        $state.go("livro", {
            action: "add",
            livroId: ""
        });
    };

    $scope.verTodos = function(){
        $state.go('livros');
    };

    this.list = function() {
        LivrosService.getAll().then(function(response) {
            if(response.status){
                $scope.livros = response.data;
            }
        });
    };

    this.getItem = function(livroId) {
        LivrosService.getItem(livroId).then(function(response) {
            if (response.status == 200) {
                $scope.livro = response;
            } else {
                // erro ao carregar livro
            }
        });
    };

    this.add = function(livro) {
        $scope.alertDanger = {};
        $scope.alertSuccess = {};

        LivrosService.register(livro).then(function(response) {
            if(response.status){
                $scope.alertSuccess = {
                    status: true,
                    msg: response.msg
                }
            } else {
                $scope.alertDanger = {
                    status: true,
                    msg: response.msg
                }
            }
        });
    };

    this.edit = function(livro) {
        LivrosService.edit(livro).then(function(response) {
            if (response.status == 200) {
                // alerta oferecendo voltar para lista ou permanecer editando
            } else {
                // erro ao editar livro
            }
        });
    };

    this.delete = function(livroId) {
        LivrosService.delete(livroId).then(function(response) {
            // sucesso

            _self.list();
        });
    };

    this.init = function() {
        $scope.livros = false;
        $scope.livro = {};

        switch ($state.current.name) {
            case "livros":
                _self.list();
                break;
            case "livro":
                if($stateParams.action == "edit" && ($stateParams.livroId != "" && !isNaN($stateParams.livroId))){
                    _self.getItem($stateParams.livroId);
                } else if ($stateParams.action == "add") {

                } else {
                    $scope.adicionar();
                }
                break;
            default:
                $state.go("livros");
        }
    }

    this.init();
});
