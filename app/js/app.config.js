app.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('livros', {
            url: '/livros',
            views: {
                content: {
                    templateUrl: '../templates/livros.html',
                    controller: 'LivrosController'
                }
            }
        })

        .state('livro', {
            url: '/livro/:action/:livroId',
            views: {
                content: {
                    templateUrl: '../templates/livro.html',
                    controller: 'LivrosController'
                }
            }
        })

    $urlRouterProvider.otherwise('/livros');
});
