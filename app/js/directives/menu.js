app.directive('menu', ['MenuService', function(MenuService, $compile) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            MenuService.getMenuItens().then(function(response, $compile) {
                var buildItemMenu = function(item, parentElement){
                    var a = document.createElement("A");

                    a.innerHTML = item.desc;
                    a.setAttribute('href', item.url);

                    parentElement.appendChild(a);
                }

                var getSubMenu = function(item, parentElement){
                    var ul = document.createElement("UL");

                    if(attrs.navbar){
                        ul.className = "hidden-sm hidden-md hidden-lg nav navbar-nav";
                    }
                    parentElement.appendChild(ul);

                    for (var i = 0; i < item.length; i++) {
                        var li = document.createElement("LI");

                        ul.appendChild(li);

                        buildItemMenu(item[i], li);

                        if(item[i].sub && item[i].sub.length > 0){
                            getSubMenu(item[i].sub, li);
                        }
                    }
                };

                if(attrs.menu != "") {
                    for (var i = 0; i < response.length; i++) {
                        if(response[i].desc == attrs.menu){
                            getSubMenu(response[i].sub, element[0]);
                            break;
                        }
                    }
                } else {
                    getSubMenu(response, element[0]);
                }
            });
        }
    }
}]);
