app.service('MenuService', function($http) {
    var _self = {};

    _self.getMenuItens = function() {
        var url = "/js/static/menu.json";

        return $http.get(url).then(function(response) {
            return response.data;
        }, function(response) {
            return response.data;
        });
    };

    return _self;
});
