app.service('LivrosService', function($http, GlobalsFactory) {
    var _self = this;

    this.getAll = function() {
        var url = GlobalsFactory.apiURL;

        return $http.get(url).then(function(response) {
            return {
                status: true,
                data: response.data
            };
        }, function(response) {
            return {
                status: false,
                msg: response.data
            };
        });
    };

    this.register = function(livro) {
        var url = GlobalsFactory.apiURL;

        var data = livro;

        return $http.post(url, data).then(function(response) {
            return {
                status: true,
                msg: response.data
            };
        }, function(response) {
            return {
                status: false,
                msg: response.data
            };
        });
    };

    this.delete = function(livroId){
        var url = GlobalsFactory.apiURL + "?id=" + livroId;

        return $http.delete(url).then(function(response) {
            return {
                status: true,
                data: response.data
            };
        }, function(response) {
            return {
                status: false,
                msg: response.data
            };
        });
    };

    return _self;
});
