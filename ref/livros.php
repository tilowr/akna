<?php

header("Access-Control-Allow-Origin: *");

$conn = new PDO("mysql:host=127.0.0.1;dbname=loja_virtual", "root", "");
$_POST = (array) json_decode(file_get_contents("php://input"));
if (strtolower($_SERVER['REQUEST_METHOD']) == "options")  {
	header("Access-Control-Allow-Origin: *");
	header('Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type, Access-Control-Allow-Methods');
	header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, DELETE');
	exit;
}

if (strtolower($_SERVER['REQUEST_METHOD']) == "get")  {
	$response = array();
	$return = array();

	$stmt = $conn->query("SELECT * FROM livros");

	while($row = $stmt->fetch()) {
	    foreach ($row as $key => $value) {
	    	if (!is_int($key)) {
	    		$return[$key] = utf8_encode($value);
	    	}
	    }

	    $response[] = $return;
	}

	echo json_encode($response);

	http_response_code(200);
	exit;
}

if (strtolower($_SERVER['REQUEST_METHOD']) == "post")  {
	if (empty($_POST["titulo"]) || empty($_POST["autor"]) || empty($_POST["preco"])) {
		http_response_code(400);
		echo json_encode("Todos os campos precisam ser preenchidos.");
		exit;
	}

	$stmt = $conn->prepare("SELECT * FROM livros WHERE titulo = :titulo");
	$stmt->bindValue(':titulo', $_POST["titulo"]);

	$stmt->execute();

	if ($stmt->rowCount()) {
		http_response_code(409);
		echo json_encode("Já existe um livro cadastrado com esse titulo.");
		exit;
	}

	$preco = str_replace(",", ".", $_POST["preco"]);
	$preco = number_format($preco, 2);

	$stmt = $conn->prepare("INSERT INTO livros (titulo, autor, preco) VALUES(:titulo, :autor, :preco)");
	$stmt->bindValue(':titulo', $_POST["titulo"]);
	$stmt->bindValue(':autor', $_POST["autor"]);
	$stmt->bindValue(':preco', $preco);

	$stmt->execute();

	echo json_encode("Livro cadastrado com sucesso");

	http_response_code(200);
	exit;
}

if (strtolower($_SERVER['REQUEST_METHOD']) == "delete")  {
	$id = $_GET["id"];

	if (empty($id)) {
		json_encode("Nenhum id encontrado");

		http_response_code(400);
		exit;
	}

	$stmt = $conn->prepare("DELETE FROM livros WHERE id = :id");
	$stmt->bindValue(':id', $id);

	if ($stmt->execute()) {
		echo json_encode("Livro deletado com sucesso");

		http_response_code(200);
		exit;
	}
}
