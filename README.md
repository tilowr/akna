# PROVA DE QUALIFICAÇÃO PARA A VAGA DE FRONTEND - AKNA #

Uma loja online vende livros, eletrodomésticos e jogos.
Crie o front do sistema do módulo da livraria, onde é possível cadastrar, listar e remover livros do estoque.

## ESPECIFICAÇÕES DO PROJETO ##

* Crie o projeto utilizando um dos seguintes frameworks JavaScripts: AngularJS, Angular 2/4, ReactJS ou VueJS.

## ORIENTAÇÕES GERAIS ##

Para essa tarefa criamos uma API REST em PHP que deverá ser utilizada. Essa API está preparada para receber dados como o formato JSON, assim como responder nesse mesmo formato.

### Para instalar essa API você deve seguir os seguintes passos: ###

* Instalar o Banco de dados MySQL 5.x;
* Executar o script SQL (livros.sql) para criar o banco de dados e as tabelas;
* Instalar o PHP (com a lib mysql e PDO);
* Descompactar o conteúdo do arquivo livros.zip em uma pasta de sua preferência;
* Rodar o seguinte comando no seu terminal favorito dentro desta pasta: “php -S 0.0.0.0:8080”;
* Para acessar essa API basta utilizar a seguinte URL localhost:8080/livros.php;

## FUNCIONALIDADES DO PROJETO ##

### Cadastro ###

Os campos para cadastrar um livro são:

Título deve estar preenchido e aceitar somente letras Autor deve estar preenchido e aceitar somente letras Preço deve estar preenchido e aceitar somente números Adicionar um botão “Cadastrar”. No click, salvar a informação no banco utilizando a API. 
 
O JSON do POST deve seguir esse formato:
``` javascript
{ 
      "titulo":"nomedolivro", 
      "autor": "autor do livro", 
      "preco":"99,99"
}
```

### Listagem ###

* Os campos a serem exibidos na listagem são os mesmos do cadastro.
* Cada item da listagem deve ter um botão “Excluir”. No click, excluir o item utilizando a API (DELETE), enviando um campo “id” como parâmetro na URL. Ex.: http://localhost/livros.php?id=99

### Menu ###

* Utilize o arquivo menu.json que foi enviado no email para montar o menu.
* Se desejar, copie o conteúdo do arquivo e cole diretamente em seu código, ou acesse-o externamente.
* Considerar que qualquer alteração feita no JSON (adicionar/remover uma categoria/subcategoria) deve re etir no menu automaticamente
* As páginas apontadas nas URLS do JSON não precisam existir, mas precisam estar linkadas (href) em seus respectivos itens do menu. 

O modelo do menu deve ficar assim:

Principal

* Livros 
      - Literatura Estrangeira 
      - Literatura Nacional
      - Auto-ajuda
      - Religião 
* Eletrodomesticos
      - Fogão 
      - Geladeira
* Jogos 
      - XBOX
      - Playstation
        - PS2
        - PS3
        - PS4
        - Jogos para PC

## PUBLICAÇÃO ##

Comitar e publicar o projeto de forma pública no github ou bitbucket e enviar o link por e-mail para [dep.desenvolvimento@akna.com](dep.desenvolvimento@akna.com).